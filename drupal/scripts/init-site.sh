if [ ! -f web/sites/default/settings.local.php ]; then
  #create settings.local.php and then add new settings to it
  cp web/sites/example.settings.local.php web/sites/default/settings.local.php
  cat <<'__END__' >> web/sites/default/settings.local.php
$databases['default']['default'] = array (
  'database' => 'drupal',
  'username' => 'drupal',
  'password' => 'drupal',
  'prefix' => '',
  'host' => 'db',
  'port' => '3306',
  'namespace' => 'Drupal\\Core\\Database\\Driver\\mysql',
  'driver' => 'mysql',
);
#generate new one with ...
#drush php-eval 'echo \Drupal\Component\Utility\Crypt::randomBytesBase64(55) . "\n";'
$settings['hash_salt'] = 'raxq0rWhqf4PNMI4MqYiZrMEipdtohqYsiW0r5aqWclI4u1QlXB40zYpGwrO867KpI14Jxc88Q';

$settings['config_sync_directory'] = "../config/sync";
__END__
  #create settings.php
  cp web/sites/default/default.settings.php web/sites/default/settings.php
  #add code to load settings.local.php
  cat >> web/sites/default/settings.php <<'__END__'
if (file_exists($app_root . '/' . $site_path . '/settings.local.php')) {
  include $app_root . '/' . $site_path . '/settings.local.php';
}
__END__
fi

echo "If you restored from database backup files in db-init then you can now visit your site."
echo "Otherwise, you can run 'drush site-install' first to initialize Drupal"
