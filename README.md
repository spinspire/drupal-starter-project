# Deprecated Project

Replaced by https://github.com/spinspire/drupal-docker

# Drupal Starter Project

Use this project as a starting point for your Drupal projects. It has the following features:

- Very lightweight. Ideal for local development.
- Dockerized: It uses 3 containers - web, php, db
- `docker-compose.override.yml` file for public HTTPS sites with Traefik
- Includes `composer-2`, `drush`, and `drupal console`
- PROD fallback: Allows your local dev environment to redirect requests for uploaded file assets to the PROD instance. That way you don't have to copy all of them from PROD to local.

# Steps to use it

- `cp .env.example .env` and then edit .env to your taste.
- Edit email addresses and site URL in `drupal/drush/drush.yml`
- Optionally: Copy database dump files (`*.sql` and `*.sql.gz`) into `db-init`
- Bring up the containers ...

```
docker-compose up -d
```

- Setup your `php` container with ...

```
docker-compose exec --user root php sh /var/www/scripts/init-container.sh
docker-compose exec php composer install
```

- Wait for db container to be ready. Check with ...

```
docker-compose logs -f db
```

- If you did not restore an existing site, then create a new one with ...

```
docker-compose exec php drush site-install
```

- Now visit your website. Either use the public URL or use the following to find the Docker IP's URL.

```
echo "http://$(docker inspect -f '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}' $(docker-compose ps -q web))"
```

# Troubleshooting

- See what containers are running:

```
docker-compose ps
```

- Look at the logs

```
docker-compose logs
```

- Look at `php` logs

```
docker-compose logs php
```

- Tail the `php` logs

```
docker-compose logs -f php
```

# Optional Steps

You can add some useful extensions with ...

```
docker-compose exec php composer require drupal/admin_toolbar drupal/module_filter drupal/pathauto
docker-compose exec php drush -y en admin_toolbar_tools module_filter pathauto
```

And then configure Pathauto patterns.

If you want to install `xdebug` extension for debugging, run this _as root_ (start shell with `docker-compose exec --user root php sh`).

```
apk add --no-cache --virtual .build-deps $PHPIZE_DEPS \
    && pecl install xdebug \
    && docker-php-ext-enable xdebug \
    && apk del -f .build-deps
```

This should create a file `/usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini`. Append the following to it.

```
xdebug.mode = debug
xdebug.start_with_request = yes
xdebug.client_port = 9000
xdebug.client_host = 172.17.0.1
```

Here the client_host should point to the docker host IP. And then don't forget to restart the `php` container so that `php-fpm` reads the changes.
